
public class Method {



	public static void main(String[] args) {
		//(例題1)のメソッドの動作検証
		printHello();

		//(例題2)のメソッド動作検証
		printPI();

		//(例題3)のメソッド動作検証
		printRandomMessage();

		//(例題4)のメソッド動作検証
		printMessage("Hello");

		//(例題5)のメソッド動作検証
		printCircleArea(2.0);

		//(例題6)のメソッド動作検証
		printRandomeMessage("なつみ");

		//(例題7)のメソッド動作検証
		printMessage("Hello",5);
	}

	//(例題1)
	static void printHello() {
		System.out.println("Hello");
	}

	//(例題2)
	static void printPI() {
		System.out.println(Math.PI);
	}

	//(例題3)
	static void printRandomMessage() {
		int n = (int)(3 * Math.random());
		String[] aisatsu = {"こんばんは","こんにちは","おはよう"};
		System.out.println(aisatsu[n]);
	}

	//(例題4)
	static void printMessage(String message) {
		System.out.println(message);
	}

	//(例題5)
	static void printCircleArea(double radius) {
		System.out.println(radius);
	}

	//(例題6)
	static void printRandomeMessage(String name) {
		int n = (int)(3 * Math.random());
		String[] greetingName = {"こんばんわ" + name +"さん","こんにちわ" + name + "さん","おはよう" + name + "さん"};
		System.out.println(greetingName[n]);
	}

	//(例題7)
	static void printMessage(String message,int count) {
		for(int i=0; i<count; ++i) {
		System.out.println(message);
		}
	}
}
